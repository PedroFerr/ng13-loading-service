import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { delay, finalize, Observable } from "rxjs";
import { LoadingService } from "./services/loading.service";

@Injectable()
export class Interceptor implements HttpInterceptor {

    constructor(private loader: LoadingService) { }

    intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
        this.loader.show();
        
        return next
            .handle(request)
            .pipe(
                delay(1999),
                finalize(() => {
                    this.loader.hide();
                })
            );
    }
}