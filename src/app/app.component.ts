import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { LoadingService } from './services/loading.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'ng13-loading-service';
    loading$ = this.loader.loading$;

    constructor(
        public loader: LoadingService,
        public http: HttpClient
    ) { }

    fetchMultipleData() {
        this.http
            .get('https://api.github.com/users/pedroferr')
            .subscribe((res) => {
                console.log(res);
            });

        this.http
            .get('https://api.github.com/users')
            .subscribe((res) => {
                console.log(res);
            });
    }
}
